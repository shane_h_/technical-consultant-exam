var payload = {};
var usernames = $('.user_name');
payload['name'] = usernames[0].outerText;
var total = $('.price_c2');
payload['price'] = total[0].outerText;
var details = $('.flights_item');
var entries = [];
for (i in details){
    entry = {};
    if(details[i].childNodes != null){
        raw = details[i].childNodes[0].nextElementSibling.outerText;
        rawSplit = raw.split(' ');
        entry['origin'] = rawSplit[0];
        entry['dest'] = rawSplit[1];
        entry['dep'] = details[i].childNodes[4].nextElementSibling.children[0].outerText;
        entry['arr'] = details[i].childNodes[4].nextElementSibling.children[1].outerText;
    }
    if(!(jQuery.isEmptyObject(entry))){
        entries.push(entry);
    }
}
payload['flightDetails'] = entries;

// POST to api
let apiurl = 'api.capturedata.ie';
headers = {
    encoding: 'html'
}
axios.post(apiurl, payload, {headers})
        .then((response) => {
            console.log(response.data);
        })
        .catch((e) => {
            console.log(e);
        })